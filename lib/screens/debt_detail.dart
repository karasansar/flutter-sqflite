import 'package:debt_reminder/data/dbHelper.dart';
import 'package:debt_reminder/models/Debt.dart';
import 'package:debt_reminder/screens/debt_update.dart';
import 'package:flutter/material.dart';

enum Options { delete, update }

// ignore: must_be_immutable
class DebtDetail extends StatefulWidget {
  Debt debt;
  DebtDetail(this.debt);
  @override
  State<StatefulWidget> createState() {
    return _debtDetailState(debt);
  }
}

// ignore: camel_case_types
class _debtDetailState extends State {
  Debt debt;
  _debtDetailState(this.debt);
  var dbHelper = DbHelper();
  var txtTitle = TextEditingController();
  var txtDescription = TextEditingController();
  var txtPersonToPay = TextEditingController();
  var txtPrice = TextEditingController();
  var txtEndsDate = TextEditingController();
  @override
  void initState() {
    txtTitle.text = debt.title;
    txtDescription.text = debt.description;
    txtPersonToPay.text = debt.personToPay;
    txtPrice.text = debt.price.toString();
    txtEndsDate.text = debt.endsDate.substring(0, 10);
    super.initState();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Borç Detayı: ${debt.title}"),
        actions: [
          PopupMenuButton<Options>(
              onSelected: selectProcess,
              itemBuilder: (BuildContext context) => <PopupMenuEntry<Options>>[
                    PopupMenuItem<Options>(
                        value: Options.delete, child: Text("Sil")),
                    PopupMenuItem<Options>(
                        value: Options.update, child: Text("Güncelle"))
                  ]),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            buildTitleFiled(),
            buildDescriptionField(),
            buildPersonToPayField(),
            buildPriceField(),
            buildEndsDateField(),
          ],
        ),
      ),
    );
  }

  TextFormField buildTitleFiled() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borcun belirtme başlığı", hintText: "Vergi Borcu"),
      controller: txtTitle,
      enabled: false,
    );
  }
  TextFormField buildDescriptionField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borcun belirtme detayı", hintText: "Vergi Borcu Detayım...."),
      controller: txtDescription,
      enabled: false,
    );
  }
  TextFormField buildPersonToPayField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Ödenecek kişi veya kurum", hintText: "Doğukan K."),
      controller: txtPersonToPay,
      enabled: false,
    );
  }
  TextFormField buildPriceField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borç miktarı", hintText: "30.0"),
      controller: txtPrice,
      enabled: false,
    );
  }

  TextFormField buildEndsDateField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Bitiş Tarihi"),
      controller: txtEndsDate,
      enabled: false,
    );
  }

  void selectProcess(Options options) async {
    switch (options) {
      case Options.delete:
        await dbHelper.delete(debt.id);
        Navigator.pop(context, true);
        break;
      case Options.update:
       bool result =  await Navigator.push(
            context, MaterialPageRoute(builder: (context) => DebtUpdate(debt)));
        break;
      default:
    }
  }
}
