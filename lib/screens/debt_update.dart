import 'package:debt_reminder/data/dbHelper.dart';
import 'package:debt_reminder/models/Debt.dart';
import 'package:debt_reminder/screens/debt_list.dart';
import 'package:flutter/material.dart';

class DebtUpdate extends StatefulWidget {
  Debt debt;
  DebtUpdate(this.debt);
  @override
  State<StatefulWidget> createState() {
    return _stateDebtUpdate(debt);
  }

}

class _stateDebtUpdate extends State {
  Debt debt;
  _stateDebtUpdate(this.debt);
  var dbHelper = DbHelper();
  var txtTitle = TextEditingController();
  var txtDescription = TextEditingController();
  var txtPersonToPay = TextEditingController();
  var txtPrice = TextEditingController();
  var txtEndsDate = TextEditingController();
  DateTime selectedDate = DateTime.now();
  @override

  void initState() {
    txtTitle.text = debt.title;
    txtDescription.text = debt.description;
    txtPersonToPay.text = debt.personToPay;
    txtPrice.text = debt.price.toString();
    txtEndsDate.text = debt.endsDate.substring(0, 10);
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Güncelle: ${debt.title}"),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            buildTitleFiled(),
            buildDescriptionField(),
            buildPersonToPayField(),
            buildPriceField(),
            buildEndsDateField(),
            buildSaveButton()
          ],
        ),
      ),
    );
  }
  TextFormField buildTitleFiled() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borcun belirtme başlığı", hintText: "Vergi Borcu"),
      controller: txtTitle,
    );
  }
  TextFormField buildDescriptionField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borcun belirtme detayı", hintText: "Vergi Borcu Detayım...."),
      controller: txtDescription,
    );
  }
  TextFormField buildPersonToPayField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Ödenecek kişi veya kurum", hintText: "Doğukan K."),
      controller: txtPersonToPay,
    );
  }
  TextFormField buildPriceField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borç miktarı", hintText: "30.0"),
      controller: txtPrice,
    );
  }

  TextFormField buildEndsDateField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Bitiş Tarihi", hintText: txtEndsDate.text),
      onTap: () => openDate(context),
      controller: txtEndsDate,
    );
  }

  buildSaveButton() {
    return FlatButton(onPressed: (){
      updateDebt();
    }, child: Text("Güncelle"));
  }

  void updateDebt() async {
    var result = await dbHelper.update(Debt.withId(id: debt.id, title: txtTitle.text, description: txtDescription.text, personToPay: txtPersonToPay.text, price: double.tryParse(txtPrice.text), status: "Aktif", endsDate: txtEndsDate.text));
    Navigator.push(context, MaterialPageRoute(builder: (context) => DebtList()));
  }

  void openDate(BuildContext context) async{
    final DateTime picked =  await showDatePicker(context: context, initialDate: selectedDate, firstDate: DateTime(2021), lastDate: DateTime(2030));
    if(picked != null){
      setState(() {
        selectedDate = picked;
        txtEndsDate.text = picked.toString();
      });
    }
  }
}