import 'package:debt_reminder/data/dbHelper.dart';
import 'package:debt_reminder/models/Debt.dart';
import 'package:debt_reminder/screens/DebtAdd.dart';
import 'package:debt_reminder/screens/debt_detail.dart';
import 'package:flutter/material.dart';

class DebtList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _debtListState();
  }
}

class _debtListState extends State {
  var dbHelper = DbHelper();
  List<Debt> debts;
  int debtCount = 0;

  @override
  void initState() {
    getDebts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Borç Listesi"),
      ),
      body: buildDebtList(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          goToDebtAdd();
        },
        child: Icon(
          Icons.add,
        ),
        tooltip: "Yeni borç ekle",
      ),
    );
  }

  ListView buildDebtList() {
    return ListView.builder(
        itemCount: debtCount,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.white70,
            elevation: 2.0,
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.white60,
                child: Text(this.debts[index].personToPay.substring(0,2)),
              ),
              title: Text(this.debts[index].title + " - " +this.debts[index].endsDate.substring(0, 10)),
              subtitle: Text(this.debts[index].description),
              onTap: () {
                goToDetail(this.debts[index]);
              },
            ),
          );
        });
  }

  void getDebts() async {
    var debtsFuture = dbHelper.getDebts();
    return debtsFuture.then((data) {
      setState(() {
        this.debts = data;
        debtCount = data.length;
      });
    });
  }

  void goToDebtAdd() async {
    bool result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => DebtAdd()));
    if (result != null) {
      if (result) {
        getDebts();
      }
    }
  }

  void goToDetail(Debt debt) async {
    bool result = await Navigator.push(context, MaterialPageRoute(builder: (context) => DebtDetail(debt)));
    if(result != null) {
      if(result){
        getDebts();
      }
    }
  }
}
