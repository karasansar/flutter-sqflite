import 'package:debt_reminder/data/dbHelper.dart';
import 'package:debt_reminder/models/Debt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DebtAdd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _addDebtState();
  }

}

class _addDebtState extends State {
  var dbHelper = DbHelper();
  var txtTitle = TextEditingController();
  var txtDescription = TextEditingController();
  var txtPersonToPay = TextEditingController();
  var txtPrice = TextEditingController();
  var txtEndsDate = TextEditingController();
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Borç Ekle"),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            buildTitleFiled(),
            buildDescriptionField(),
            buildPersonToPayField(),
            buildPriceField(),
            buildEndsDateField(),
            buildSaveButton()
          ],
        ),
      ),
    );
  }

  TextFormField buildTitleFiled() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borcun belirtme başlığı", hintText: "Vergi Borcu"),
      controller: txtTitle,
    );
  }
  TextFormField buildDescriptionField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borcun belirtme detayı", hintText: "Vergi Borcu Detayım...."),
      controller: txtDescription,
    );
  }
  TextFormField buildPersonToPayField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Ödenecek kişi veya kurum", hintText: "Doğukan K."),
      controller: txtPersonToPay,
    );
  }
  TextFormField buildPriceField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Borç miktarı", hintText: "30.0"),
      controller: txtPrice,
    );
  }
  TextFormField buildEndsDateField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Bitiş Tarihi", hintText: txtEndsDate.text),
      onTap: () => openDate(context),
    );
  }

  buildSaveButton() {
    return FlatButton(onPressed: (){
      addDebt();
    }, child: Text("Ekle"));
  }

  void addDebt()  async{
   var result = await dbHelper.insert(Debt(title: txtTitle.text, description: txtDescription.text, personToPay: txtPersonToPay.text, price: double.tryParse(txtPrice.text), status: "Aktif", endsDate: txtEndsDate.text));
   Navigator.pop(context, true);
  }

  void openDate(BuildContext context) async{
    final DateTime picked =  await showDatePicker(context: context, initialDate: selectedDate, firstDate: DateTime(2021), lastDate: DateTime(2030));
    if(picked != null){
      setState(() {
        selectedDate = picked;
        txtEndsDate.text = picked.toString();
      });
    }
  }
}