class Debt {
  int id;
  String title;
  String description;
  String personToPay;
  double price;
  String status;
  String endsDate;

  Debt(
      {this.title,
      this.description,
      this.personToPay,
      this.price,
      this.status, this.endsDate});
  Debt.withId(
      {this.id,
      this.title,
      this.description,
      this.personToPay,
      this.price,
      this.status, this.endsDate});

  Map<String,dynamic> toMap() {
    var map = Map<String,dynamic>();
    map["title"] = title;
    map["description"] = description;
    map["person_to_pay"] = personToPay;
    map["price"] = price;
    map["status"] = status;
    map["ends_date"] = endsDate;
    if(id!=null) {
      map["id"] = id;
    }
    return map;
  }

  Debt.fromObject(dynamic o) {
    this.id = int.tryParse(o["id"].toString());
    this.title = o["title"];
    this.description = o["description"];
    this.personToPay = o["person_to_pay"];
    this.price = double.tryParse(o["price"].toString());
    this.status = o["status"];
    this.endsDate = o["ends_date"];
  }
}
