import 'dart:async';

import 'package:debt_reminder/models/Debt.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  Database _db;

  Future<Database> get db async{
    if(_db == null) {
      _db = await innitalizeDb();
    }
    return _db;
  }

  Future<Database> innitalizeDb() async{
    String dbPath = join(await getDatabasesPath(), "debts_reminder.db");
    var debtsReminder = await openDatabase(dbPath, version: 1, onCreate: createDb/*, onUpgrade: onUpgrade*/);
    return debtsReminder;
  }

  Future<void> createDb(Database db, int version) async {
    await db.execute("Create table debts(id integer primary key, title text, description text, person_to_pay text, price decimal, status text, ends_date text)");
  }

  Future<List<Debt>> getDebts() async {
    Database db = await this.db;
    var result = await db.query("debts");
    return List.generate(result.length, (index){
     return  Debt.fromObject(result[index]);
    });
  }

  Future<int> insert(Debt debts) async{
    Database db = await this.db;
    var result = await db.insert("debts", debts.toMap());
    return result;
  }

  Future<int> delete(int id) async{
    Database db = await this.db;
    var result = await db.rawDelete("delete from debts where id = $id");
    return result;
  }

  Future<int> update(Debt debt) async{
    Database db = await this.db;
    var result = await db.update("debts", debt.toMap(), where: "id=?", whereArgs: [debt.id]);
    return result;
  }

  /*Future<void> onUpgrade(Database db, int oldVersion, int newVersion){
   db.execute("ALTER TABLE debts ADD COLUMN ends_date text");
  }*/
}